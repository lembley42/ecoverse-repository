﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour {

    public List<SubsistenceOperation> subsistence = new List<SubsistenceOperation>();
    public List<MiningOperation> mines = new List<MiningOperation>();
    public List<FarmingOperation> farms = new List<FarmingOperation>();
    public List<FactoryOperation> factories = new List<FactoryOperation>();
    public List<ConstructionOperation> constructions = new List<ConstructionOperation>();

    public List<SystemMarket> markets = new List<SystemMarket>();
    public List<ColonyJobMarket> jobMarkets = new List<ColonyJobMarket>();


    void Start()
    {
        DayOne();
        InvokeRepeating("Cycle", 1f, 1f);
    }

    void Cycle()
    {
        Operations();
        Updates();
    }

    void DayOne()
    {
        Updates();
    }

    void Operations()
    {
        /* Not finished
        foreach (SubsistenceOperation operation in subsistence)
            operation.StartSubsistenceOperation();
        */
        foreach (MiningOperation operation in mines)
            operation.StartMiningOperation();

        foreach (FarmingOperation operation in farms)
            operation.StartFarmingOperation();

        foreach (FactoryOperation operation in factories)
            operation.StartFactoryOperation();

        foreach (ConstructionOperation operation in constructions)
            operation.StartConstructionOperation();
    }

    void Updates()
    {
        foreach (SystemMarket market in markets)
            market.UpdateMarketPrices();

        foreach (ColonyJobMarket jobMarket in jobMarkets)
            jobMarket.UpdateWagesAndCoverage();
    }
}
