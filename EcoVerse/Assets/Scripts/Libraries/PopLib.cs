﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public static class PopLib {

    public const int countOfEducation = 3;
    public enum EducationEnum
    {
        Worker,
        Skilled_Worker,
        Academic
    }

    [Serializable]
    public class Citizen
    {
        public EducationEnum education;
        public float size;        
        public float limit;

        public float employmentDemand;
        public float employmentCoverage;

        public float cash;

        public float wage;
        public float minWage;

        public Dictionary<EcoLib.Commodities, Need> needs;
    }

    [Serializable]
    public class Need
    {
        public float supply;
        public float demanded;
        public float coverage;
    }

    [Serializable]
    public class Employee
    {
        public EducationEnum education;
        public float employing;
    }

    public class Demand
    {
        public EcoLib.Commodities commodity;
        public float required;
        public float coverage;
    }
}
/*
Bildungsstand
Größe
Maximale Größe
Wachstum

Cash
Lohn
Mindestlohn
Arbeitsbedarf
Arbeitsabdeckung

Nahrungsbedürfnis
Nahrungsabdeckung

Konsumerbedürfnisse
Konsumerabdeckung
    */