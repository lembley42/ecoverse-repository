﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public static class EcoLib {

    public const int countOfCommodities = 17;
    public enum Commodities
    {
        Food,
        Cotton,
        Spices,
        Chemical_Minerals,
        Oil,
        Silicium,
        Copper,
        Iron,
        Coal,
        Clay,
        Clothing,
        Steel,
        Cement,
        Consumer_Hygenics,
        Plastics,
        Electronic_Components,
        Consumer_Electronics,
        Machines
    }

    [Serializable]
    public class Market
    {
        public Market()
        {
            price = 100f;
            minPrice = 10f;
            maxPrice = 1000f;
        }

        public string name;

        public float price;
        public float minPrice;
        public float maxPrice;

        public float totalStored;
        public List<MarketStorage> storages;

        public float supplyToday;
        public float demandToday;
    }

    [Serializable]
    public class Input
    {
        public string name;
        public Commodities commodity;
        public float required;
        public float stored;
        public float maxStorage;
    }
}
