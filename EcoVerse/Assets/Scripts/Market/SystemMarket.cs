﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class SystemMarket : MonoBehaviour {

    public EcoLib.Market[] markets = new EcoLib.Market[EcoLib.countOfCommodities];

    #region Initializiation
    private void Start()
    {
        for(int i = 0; i < markets.Length; i++)
            markets[i].name = Enum.GetName(typeof(EcoLib.Commodities), i);
    }
    #endregion

    public void UpdateMarketPrices()
    {
        foreach(EcoLib.Market market in markets)
        {
            float supply = market.supplyToday;
            float demand = market.demandToday;
            float stored = market.totalStored;

            market.price -= supply * 0.05f;
            market.price += demand * 0.05f;

            market.supplyToday = 0;
            market.demandToday = 0;
        }
    }

    public void BuyGood(IncomeStatement incomeStatement, EcoLib.Input storage, EcoLib.Commodities commodity, float demand, Company company)
    {
        EcoLib.Market market = markets[(int)commodity];
        if (company.cash <= 0f || market.totalStored == 0)
            return;

        float available = market.totalStored;
        float affordable = company.cash / market.price;
        float bought = Mathf.Min(available, demand, affordable);

        storage.stored += bought;
        WithdrawFromStorages(commodity, bought);
        market.demandToday += demand;

        company.cash -= bought * market.price;
        incomeStatement.costsOfInputs += bought * market.price;
    }

    public void BuyConsumerGood(PopLib.Citizen citizen, EcoLib.Commodities commodity, float demand, float cash)
    {
        EcoLib.Market market = markets[(int)commodity];
        float available = market.totalStored;
        float affordable = cash / market.price;
        float bought = Mathf.Min(available, affordable, demand);

        if (bought == 0)
            return;

        citizen.needs[commodity].supply = bought;
        WithdrawFromStorages(commodity, bought);
        citizen.cash -= bought * market.price;
    }

    void WithdrawFromStorages(EcoLib.Commodities commodity, float bought)
    {
        EcoLib.Market market = markets[(int)commodity];
        foreach (MarketStorage marketStorage in market.storages)
        {
            float share = marketStorage.stored / market.totalStored;
            float boughtFromCompany = bought * share;
            marketStorage.stored -= boughtFromCompany;
            marketStorage.GetComponent<IncomeStatement>().saleRevenue += boughtFromCompany * market.price;
        }
        market.totalStored -= bought;
    }
}