﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class FactoryStorage : MonoBehaviour {

    public List<EcoLib.Input> inputs = new List<EcoLib.Input>();

    private FactoryOperation factory;
    private SystemMarket systemMarket;
    private IncomeStatement incomeStatement;
    private Company company;

    #region Initialization
    private void Awake()
    {
        factory = GetComponent<FactoryOperation>();
        systemMarket = GetComponentInParent<SystemMarket>();
        incomeStatement = GetComponent<IncomeStatement>();
        company = GetComponent<Company>();

        InitializeMaxStorage();
    }

    void InitializeMaxStorage()
    {
        foreach (EcoLib.Input input in inputs)
            input.maxStorage = input.required * factory.baseProduction;
    }

    private void Start()
    {
        for(int i = 0; i < inputs.Count; i++)
        {
            EcoLib.Input input = inputs[i];
            input.name = Enum.GetName(typeof(EcoLib.Commodities), (int)input.commodity);
        }
    }
    #endregion

    public void BuyInputsOnMarket()
    {
        foreach(EcoLib.Input input in inputs)
        {
            float deficit = input.maxStorage - input.stored;
            if(deficit > 0)
                systemMarket.BuyGood(incomeStatement, input, input.commodity, deficit, company);
        }
    }

    public float GetInputCoverage(float production)
    {
        float inputCoverage = 1f;
        foreach(EcoLib.Input input in inputs)
        {
            float coverage = input.stored / (input.required * production);
            if (coverage < inputCoverage)
                inputCoverage = coverage;
        }
        return inputCoverage;
    }

    public void ConsumeInputs(float production)
    {
        foreach(EcoLib.Input input in inputs)
        {
            float consume = input.required * production;
            input.stored -= consume;
        }
    }
}