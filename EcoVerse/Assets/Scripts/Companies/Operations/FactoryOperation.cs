﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FactoryOperation : MonoBehaviour {

    public EcoLib.Commodities producingCommodity;
    public float baseProduction;
    public float productionPotential;

    private MarketStorage marketStorage;
    private FactoryStorage factoryStorage;
    private Employment employment;

    #region Initialization
    private void Awake()
    {
        marketStorage = GetComponent<MarketStorage>();
        marketStorage.commodity = producingCommodity;

        factoryStorage = GetComponent<FactoryStorage>();
        employment = GetComponent<Employment>();
    }
    #endregion

    public void StartFactoryOperation()
    {
        productionPotential = employment.GetEmploymentCoverage();
        float productionOutput = baseProduction * productionPotential;

        float inputCoverage = factoryStorage.GetInputCoverage(productionOutput);
        productionPotential *= inputCoverage;

        productionOutput = baseProduction * productionPotential;

        marketStorage.AddProductionToStorage(productionOutput);
        factoryStorage.ConsumeInputs(productionOutput);
        factoryStorage.BuyInputsOnMarket();

        employment.PayEmployees();
    }
}
