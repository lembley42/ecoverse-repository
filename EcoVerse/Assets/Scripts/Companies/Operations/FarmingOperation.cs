﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FarmingOperation : MonoBehaviour {

    public EcoLib.Commodities producingCommodity;
    public float baseProduction;
    public float productionPotential;

    private MarketStorage marketStorage;
    private Employment employment;

    #region Initialization
    private void Awake()
    {
        marketStorage = GetComponent<MarketStorage>();
        marketStorage.commodity = producingCommodity;

        employment = GetComponent<Employment>();
    }
    #endregion

    public void StartFarmingOperation()
    {
        productionPotential = employment.GetEmploymentCoverage();
        float production = baseProduction * productionPotential;
        marketStorage.AddProductionToStorage(production);

        employment.PayEmployees();
    }
}
