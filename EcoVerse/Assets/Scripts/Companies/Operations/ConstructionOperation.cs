﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConstructionOperation : MonoBehaviour {


    public GameObject constructing;
    public float baseConstruction;
    public float productionPotential;
    public float constructionProgress;

    private ConstructionStorage constructionStorage;
    private Employment employment;

    #region Initialization
    private void Awake()
    {
        constructionStorage = GetComponent<ConstructionStorage>();
        employment = GetComponent<Employment>();
    }
    #endregion

    public void StartConstructionOperation()
    {
        //Limit input requirements so that if nearly completed no resources go to waste
        productionPotential = employment.GetEmploymentCoverage();
        productionPotential = employment.GetEmploymentCoverage();
        float constructionDone = baseConstruction * productionPotential;

        float inputCoverage = constructionStorage.GetInputCoverage(constructionDone);
        productionPotential *= inputCoverage;

        constructionDone = baseConstruction * productionPotential;

        constructionProgress += constructionDone;
        if(constructionProgress >= 100)
            FinishConstruction();


        constructionStorage.ConsumeInputs(constructionDone);
        constructionStorage.BuyInputsOnMarket();
    }

    void FinishConstruction()
    {
        GameObject finishedConstruction = Instantiate(constructing, transform.parent);
        //Ownership

        DestroyObject(this.gameObject);
    }
}
