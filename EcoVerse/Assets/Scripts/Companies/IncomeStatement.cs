﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IncomeStatement : MonoBehaviour {

    public float saleRevenue;

    public float costsOfInputs;
    public float wageExpenses;
}
