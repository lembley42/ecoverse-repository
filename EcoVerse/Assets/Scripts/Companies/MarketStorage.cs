﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MarketStorage : MonoBehaviour {

    public EcoLib.Commodities commodity;
    public float stored;

    private EcoLib.Market commodityMarket;

    #region Initialization
    void Start()
    {
        commodityMarket = GetComponentInParent<SystemMarket>().markets[(int)commodity];
        commodityMarket.storages.Add(this);
    }
    #endregion

    public void AddProductionToStorage(float production)
    {
        stored += production;
        commodityMarket.supplyToday += production;
        commodityMarket.totalStored += production;
    }
}
