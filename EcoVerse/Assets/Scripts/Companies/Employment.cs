﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Employment : MonoBehaviour {

    public PopLib.Employee employees;

    private ColonyPopulation population;
    private IncomeStatement incomeStatement;
    private Company company;

    #region Initialization
    private void Awake()
    {
        population = GetComponentInParent<ColonyPopulation>();
        incomeStatement = GetComponent<IncomeStatement>();
        company = GetComponent<Company>();
    }

    private void Start()
    {
        StartEmployment();
    }

    void StartEmployment()
    {
        population.citizens[(int)employees.education].employmentDemand += employees.employing;
    }
    #endregion

    public float GetEmploymentCoverage()
    {
        return population.GetCoverage(employees.education);
    }

    public void PayEmployees()
    {
        /*
         * 
         * 
         * 
         * 
         * */

        float wage = population.GetWage(employees.education);
        float coverage = GetEmploymentCoverage();
        float pay = (employees.employing * coverage) * wage;

        incomeStatement.wageExpenses = pay;
        population.citizens[(int)employees.education].cash += pay;
    }
}