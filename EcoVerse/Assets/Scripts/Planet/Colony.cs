﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Colony : MonoBehaviour {

    public ColonyPopulation population;

    private void Awake()
    {
        population = GetComponent<ColonyPopulation>();
    }
}
