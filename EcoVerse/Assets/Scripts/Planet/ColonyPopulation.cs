﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColonyPopulation : MonoBehaviour {

    public PopLib.Citizen[] citizens = new PopLib.Citizen[3];

    #region Gets
    public float GetWage(PopLib.EducationEnum education)
    {
        return citizens[(int)education].wage;
    }

    public float GetCoverage(PopLib.EducationEnum education)
    {
        return citizens[(int)education].employmentCoverage;
    }

    public float GetSize(PopLib.EducationEnum education)
    {
        return citizens[(int)education].size;
    }
    #endregion

    /*
    void PopulationGrowth()
    {
        float monthlyGrowth = populationGrowth / 12;
        float populationChange = population * monthlyGrowth;
        
        population = Mathf.Clamp(populationChange, 0, maxPopulation);

        if (populationChange > 0)
            jobMarket.citizens[(int)PopLib.EducationEnum.Worker].size += populationChange;
        else if(populationChange < 0)
        {
            for(int i = 0; i < PopLib.countOfEducation; i++)
            {
                PopLib.Citizen citizen = jobMarket.citizens[i];
                citizen.size = Mathf.Max(citizen.size + populationChange, 0);
                populationChange -= citizen.size;

                if (populationChange <= 0)
                    break;
            }
        }
    }
    */
}