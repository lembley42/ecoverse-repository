﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColonyJobMarket : MonoBehaviour {

    private ColonyPopulation population;
    
    private void Awake()
    {
        population = GetComponent<ColonyPopulation>();
    }

    public void UpdateWagesAndCoverage()
    {
        PopLib.Citizen[] citizens = population.citizens;

        foreach(PopLib.Citizen citizen in citizens)
        {
            float employmentRate = Mathf.Clamp01(citizen.size / citizen.employmentDemand);
            float calculatedWage = employmentRate * 10;
            citizen.wage = Mathf.Max(calculatedWage, citizen.minWage);

            citizen.employmentCoverage = employmentRate;
        }
    }
}
