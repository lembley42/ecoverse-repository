﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Consumer : MonoBehaviour {

    private ColonyPopulation population;
    private SystemMarket systemMarket; 

    private void Awake()
    {
        population = GetComponent<ColonyPopulation>();
        systemMarket = GetComponentInParent<SystemMarket>();
    }

    void BuyUpFood()
    {
        foreach(PopLib.Citizen citizen in population.citizens)
        {
            PopLib.Need foodNeed = citizen.needs[EcoLib.Commodities.Food];
            float demanded = citizen.size * foodNeed.demanded;
            float cash = citizen.cash;

            systemMarket.BuyConsumerGood(citizen, EcoLib.Commodities.Food, demanded, cash);
        }
    }

    void BuyUpConsumerProducts()
    {
        foreach (PopLib.Citizen citizen in population.citizens)
        {
            foreach (var need in citizen.needs)
            {
                if (need.Key == EcoLib.Commodities.Food)
                    continue;
                if (citizen.cash == 0)
                    break;

                float demanded = citizen.size * need.Value.demanded;
                float cash = citizen.cash;

                systemMarket.BuyConsumerGood(citizen, need.Key, demanded, cash);
            }
        }
    }
}
